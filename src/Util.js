const Error = require("./Error");
const _ = require("lodash");



class Util {
    constructor() { }
    
    /**
     * @param {any} item
     * @returns {boolean}
     */
    isObject(item) {
        if (Array.isArray(item)) return false;
        if (item.constructor.name !== "Object") return false;
        if (typeof item !== "object") return false;
        return true;
    }


    /**
     * @param {any} item
     * @returns {boolean}
     */
    isString(item) {
        return typeof item !== "string" || item === "" ? false : true;
    }

    /**
     * .
     * @param {string} key
     */
    parseKey(key) {
        if (!this.isString(key)) {
            throw new Error("Key string tipli bir veri olmalıdır.");
        }
        if (key.includes(".")) {
            const parseDot = key.split(".");
            const targetKey = parseDot.shift();
            const target = parseDot.join(".");
            return { key: targetKey, target };
        }
        return { key, target: undefined };
    }

    /**
     * @param {any} item
     * @returns {boolean}
     */
    isNumber(item) {
        if (typeof item === "number") return true;
        if (typeof item === "string") {
            return item.match(/\d+$/).input ? true : false;
        }
        return false;
    }

    /**
     * @param {any} item
     * @returns {boolean}
     */
    isFunction(item) {
        return typeof item === "function" ? true : false;
    }


    /**
     * @param {string} item
     */
    parseValue(item) {
        if ((!item || item === "" ) && !this.isNumber(item)) throw new Error("Value hatalı belirtilmiş.");
        return item;
    }


    setData(key, data, value) {
        const parsed = this.parseKey(key);
        if (this.isObject(data) && parsed.target) {
            return _.set(data, parsed.target, value);
        } else if (parsed.target) throw new Error("Target'in tipi object değil.");
        return data; 
    }

    unSetData(key, data) {
        const parsed = this.parseKey(key);
        const cloneData = _.cloneDeep(data);
        if (this.isObject(data) && parsed.target) {
            _.unset(cloneData, parsed.target);
        } else if (parsed.target) throw new Error("Target'in tipi object değil.");
        return cloneData;
    }

    getData(key, data) {
        const parsed = this.parseKey(key);
        if (parsed.target) data = _.get(data, parsed.target);
        return data;
    }
}


module.exports = Util;
