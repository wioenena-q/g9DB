const chalk = require("chalk");




class g9dbError extends Error {
    constructor(message, content) {
        super(chalk.cyan(message));
        this.code = chalk.red(content || "TypeError");
    }
    
    get name() {
        return this.code;
    }
}



module.exports = g9dbError;