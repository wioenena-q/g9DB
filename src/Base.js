const { EventEmitter } = require("events");
const mongoose = require("mongoose");
const Error = require("./Error");
const util = require("./Util");

const mongoURLRegex = /^mongodb([a-z+]{0,15})?.+/g;

class Base extends EventEmitter {
  /**
   * @param {string} mongodbURL
   * @param {mongoose.ConnectionOptions} [connectionOptions]
   */
  constructor(mongodbURL, connectionOptions) {
    super();
    this.util = new util();
    // Eğer mongodb url'sine uymuyor ise hata mesajı attıyoruz.
    if (!mongodbURL || !mongodbURL.match(mongoURLRegex)) {
      throw new Error("Kullanılamayan Database url'si", "MongoError");
    }
    if (connectionOptions && !this.util.isObject(connectionOptions)) {
      throw new Error(`connectionOptions bir object tipinde değil.`);
    }
    // mongodb url'sini Base class'ına property olarak ekliyoruz.
    /**@type {string} */
    Object.defineProperty(this, "mongodbURL", {
      value: mongodbURL,
      writable: true,
    });

    // Connection ayarlarını gizliyoruz.
    /**@type {mongoose.ConnectionOptions} */
    Object.defineProperty(this, "options", {
      value: connectionOptions,
    });

    // Bağlantıyı kuruyoruz.
    /**@type {mongoose.Connection} */
    this.connection = this._createConnection();

    // Eğer bağlantı hatası alırsak
    this.connection.on("error", (e) => {
      this.emit("error", e);
    });
    this.connection.on("open", () => {
      this.readyAt = new Date();
      this.emit("ready");
    });
  }

  /**
   * Bağlantıyı başlatan metod.
   * @private
   * @param {string} [url]
   * @returns {mongoose.Connection}
   */
  _createConnection(url) {
    this.emit("debug", "Database bağlantısı başlatılıyor.");
    if (url && mongoURLRegex.test(url)) this.mongodbURL = url;
    if (!this.mongodbURL || !this.util.isString(this.mongodbURL)) {
      throw new Error("Database url'si aranıyor.", "MongoError");
    }
    delete this["options"]["useUnique"];
    // Bağlantıyı başlatıyoruz.
    return mongoose.createConnection(this.mongodbURL, {
      ...this.options,
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }

  /**
   * Bağlantıyı sonlandıran metod.
   * @private
   * @returns {Promise<void>}
   */
  async _destroyDatabase() {
    await this.connection.close(true).catch((e) => this.emit("error", e));
    this.readyAt = undefined;
    this.mongodbURL = null;
    this.emit("debug", "Database bağlantısı kapatıldı.");
    return;
  }

  // Mongodb url'sini dönüyor
  get url() {
    return this.mongodbURL;
  }

  // Bağlantı durumunu dönüyor.
  get state() {
    if (!this.connection || typeof this.connection.readyState !== "number")
      return "DISCONNECTED";
    switch (this.connection.readyState) {
      case 0:
        return "DISCONNECTED";
        break;
      case 1:
        return "CONNECTED";
        break;
      case 2:
        return "CONNECTING";
        break;
      case 3:
        return "DISCONNECTING";
        break;
    }
  }
}

module.exports = Base;
