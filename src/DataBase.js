const Base = require("./Base");
const mongoose = require("mongoose");
const SchemaManager = require("./SchemaManager");
const Error = require("./Error");
const { Collection } = require("g9-collection");
const fs = require("fs");

class Database extends Base {
    /**
     * @param {string} mongodbURL MongoDB url'niz.
     * @param {string} [name = "JSON"] model isminiz.(isteğe bağlı)
     * @param {mongoose.ConnectionOptions} [connectionOptions] connectionOptions ayarlarınız.
     */
    constructor(mongodbURL, name = "json", connectionOptions = {}) {
        super(mongodbURL, connectionOptions);
        /**@type {string} @readonly */
        this.modelName = name;
        /**@type {SchemaManager} @readonly */
        this.schemaManager = new SchemaManager(this);
        /**@type {mongoose.Model} @readonly*/
        this.schema = this.schemaManager.createSchema();
    }


    /**
     * Bağlantı kurulmuşmu kurulmamış mı kontrol ettirir.
     * @private
     * @returns {void}
     */
    checkConnnection() {
        if (this.state === "CONNECTED" || this.state === "CONNECTING") {
            return void 0;
        }
        throw new Error("Database'ye bağlanılmadan işlem yapamam.", "Bağlantı hatası");
    }

    /**
     * MongoDB bağlantısını sonlandırır.
     * @returns {Promise<void>}
     * @example db.disconnect();
     */
    async disconnect() {
        this.emit("debug", "Database bağlantısı kapatılıyor.");
        await this._destroyDatabase();
        return;
    }

    /**
     * Veri kaydedersiniz.
     * @param {string} key Key
     * @param {any} value Value
     * @returns {Promise<any>} value
     * @example db.set("test","succes").then((value) => console.log(`${value} DataBase'ye kaydedildi.`));
     */
    async set(key, value) {
        this.checkConnnection();
        const parsed = this.util.parseKey(key);
        value = this.util.parseValue(value);
        const modelCheck = await this.schemaManager.checkModel(parsed.key);
        if (modelCheck) {
            const model = await this.schemaManager.getData(parsed.key);
            model.data = parsed.target ? this.util.setData(key, Object.assign({}, model.data), value) : value;
            await model.save().catch((e) => {
                this.emit("error", e);
            });
            return model.data;
        } else {
            const model = new this.schema({
                ID: parsed.key,
                data: parsed.target ? this.util.setData(key, {}, value) : value
            });
            await model.save().catch((e) => {
                this.emit("error");
            });
            return model.data;
        }
    }

    /**
     * Veri çekersiniz.
     * @param {string} key Key
     * @returns {Promise<any>}
     * @example db.get("test").then((data) => console.log(data));
     */
    async get(key) {
        this.checkConnnection();
        const parsed = this.util.parseKey(key);
        const model = await this.schema.findOne({ ID: parsed.key })
            .catch((e) => this.emit("error", e));
        if (!model) return null;
        let data;
        if (parsed.target) data = this.util.getData(key, Object.assign({}, model.data));
        else data = model.data;
        return data;
    }

    /**
     * Veri çekersiniz.
     * @param {string} key Key
     * @returns {Promise<any>}
     * @example db.fetch("test").then((data) => console.log(data));
     */
    async fetch(key) {
        return await this.get(key);
    }

    /**
     * Veri varmı onu kontrol edersiniz.
     * @param {string} key Key
     * @returns {Promise<boolean>}
     * @example db.exists("test").then((bool) => console.log(bool));
     */
    async exists(key) {
        this.checkConnnection();
        const parsed = this.util.parseKey(key);
        return await this.schemaManager.checkModel(parsed.key) ? true : false;
    }

    /**
     * Veri varmı yokmu onu kontrol edersiniz.
     * @param {string} key Key
     * @returns {Promise<boolean>}
     * @example db.has("test").then((bool) => console.log(bool));
     */
    async has(key) {
        return await this.exists(key);
    }

    /**
     * Bütün verileri array şeklinde döndürür. Limit isteğe bağlı.
     * @param {number} [limit] Limit
     * @returns {Promise<any[]>}
     * @example db.all().then((items) => console.log(items));
     */
    async all(limit = 0) {
        this.checkConnnection();
        if (!this.util.isNumber(limit) || limit < 1) limit = 0;
        let allData = (await this.schema.find().catch((e) => { }));
        // @ts-ignore
        if (limit) allData = allData.slice(0, limit);
        // @ts-ignore
        return allData.map((item) => ({ ID: item.ID, data: item.data }));
    }

    /**
     * Bütün verileri array şeklinde döndürür. Limit isteğe bağlı.
     * @param {number} [limit] Limit
     * @returns {Promise<any[]>}
     * @example db.fetchAll().then((items) => console.log(items));
     */
    async fetchAll(limit = 0) {
        return await this.all(limit);
        
    }

    /**
     * Bütün verileri Object tipinde döndürür. Limit isteğe bağlı.
     * @param {number} [limit] Limit
     * @returns {Promise<Object>}
     * @example db.toJSON().then((object) => console.log(object));
     */
    async toJSON(limit = 0) {
        this.checkConnnection();
        const allData = await this.all(limit);
        const json = {};
        allData.map((item) => json[item.ID] = item.data);
        return json;
    }

    /**
     * Veri siler.
     * @param {string} key Key
     * @returns {Promise<void>}
     * @example db.delete("test")
     */
    async delete(key) {
        this.checkConnnection();
        const parsed = this.util.parseKey(key);
        if (!(await this.has(parsed.key))) throw new Error(`${key} ID'li veri yok silemem.`);
        const data = await this.get(parsed.key);
        if (parsed.target) {
            const unsetData = this.util.unSetData(key, data);
            return await this.set(parsed.key, unsetData);
        } else {
            await this.schemaManager.findAndDelete(parsed.key);
            return;
        }
        return;
    }

    /**
     * Bütün veriyi siler.
     * @returns {Promise<void>}
     * @example db.deleteAll().then(() => console.log(await db.fetchAll()));
     */
    async deleteAll() {
        this.checkConnnection();
        await this.schemaManager.deleteAll();
        return;
    }


    /**
     * 
     * @returns {number}
     */
    get uptime() {
        if (!this.readyAt) return 0;
        const timestamp = this.readyAt.getTime();
        return Date.now() - timestamp;
    }


    /**
     * Bağlantı sağlar.
     * @param {string} url
     * @returns {Promise<void>}
     * @example db.connect("mongo.....");
     */
    async connect(url) {
        await this._createConnection(url);
        return;
    }

    /**
     * Model ismi.
     * @returns {string}
     */
    get name() {
        return this.schema.modelName;
    }


    /**
     * Aranan verinin tipini döner.
     * @param {string} key
     * @returns {Promise<"string" | "number" | "bigint" | "boolean" | "symbol" | "array" | "undefined" | "object" | "function">}
     * @example db.type("test").then((a) => console.log(a));
     */
    async type(key) {
        this.checkConnnection();
        const data = await this.get(key);
        if (Array.isArray(data)) return "array";
        return typeof data;
    }


    /**
     * Array'den veri siler.
     * @param {string} key
     * @param {any | any[]} value
     * @param {boolean} [multiple]
     * @returns {Promise<any>}
     * @example db.pull("test","hello");
     */
    async pull(key, value, multiple = true) {
        value = this.util.parseValue(value);
        let data = await this.get(key);
        if (!data) return false;
        if (!Array.isArray(data)) throw new Error(`${key} ID'li veri array tipinde değil.`);
        if (Array.isArray(value)) {
            data = data.filter((item) => !value.includes(item));
            return await this.set(key, data);
        } else {
            if (multiple) {
                data = data.filter((item) => item !== value);
                return await this.set(key, data);
            } else {
                const hasItem = data.some((item) => item === value);
                if (!hasItem) return false;
                const index = data.findIndex((item) => item === value);
                data = data.splice(index, 1);
                return await this.set(key, data);
            }
        }
    }

    toString() {
        return `g9db:${this.schema.modelName}`;
    }

    /**
     * Yeni bir DataBase class'ı oluşturur.
     * @param {string} name Model'in ismi
     * @returns {this}
     * @example db.createModel("Guilds");
     */
    createModel(name) {
        if (!this.util.isString(name)) throw new Error(`${name} string tipinde bir parametre olmak zorundadır.`);
        return new this.constructor(this.mongodbURL, name, this["options"]);
    }

    /**
     * Value'leri array olarak döner.
     * @returns {Promise<any[]>} Values[]
     * @example db.valueArray().then((items) => console.log(items));
     */
    async valueArray() {
        const allData = await this.all();
        return allData.map((item) => item.data);
    }

    /**
     * Key'leri array olarak döner.
     * @returns {Promise<string[]>} Key[]
     * @example db.keyArray().then((items) => console.log(items));
     */
    async keyArray() {
        const allData = await this.all();
        return allData.map((item) => item.ID);
    }

    /**
     * Matematik işlemleri yapar.
     * @param {string} key Key
     * @param {"+" | "-" | "*" | "/" | "%"} operator Operator
     * @param {number} value Value
     * @param {boolean} [goToNegative] Verinin -'lere düşüp düşmeyeceği. (default false)
     * @returns {Promise<any>}
     * @example db.math("test","/",5,false);
     */
    async math(key, operator, value, goToNegative = false) {
        if (!this.util.isNumber(value)) throw new Error(`value'nin tipi sayı değil.`);
        if (value < 1) throw new Error(`value 1'den küçük olamaz`);
        value = Number(value);
        if (!(typeof goToNegative === "boolean")) throw new Error("goToNegative parametresi boolean tipte olmak zorundadır.");
        let data = await this.get(key);
        if (!data && !this.util.isNumber(data)) {
            return await this.set(key, value);
        }
        if (!this.util.isNumber(data)) throw new Error(`${key} ID'li veri number tipli bir veri değil`);
        data = Number(data);
        switch (operator) {
            case "+":
                data += value;
                return await this.set(key, data);
                break;
            case "-":
                data -= value;
                if (goToNegative === false && data < 1) data = 0;
                return await this.set(key, data);
                break;
            case "*":
                data *= value;
                return await this.set(key, data);
                break;
            case "/":
                data /= value;
                return await this.set(key, data);
                break;
            case "%":
                data %= value;
                return await this.set(key, data);
                break;
            default:
                return undefined;
                break;
        }
    }

    /**
     * Toplama işlemi yapar.
     * @param {string} key Key
     * @param {number} value Value
     * @returns {Promise<any>}
     * @example db.add("test",5,false);
     */
    async add(key, value) {
        const result = await this.math(key, "+", value);
        return result;
    }


    /**
     * Çıkarma işlemi yapar.
     * @param {string} key Key
     * @param {number} value Value
     * @param {boolean} [goToNegative] Eksilere düşüp düşmeyeceği
     * @returns {Promise<any>}
     * @example db.substr("test",2);
     */
    async substr(key, value,goToNegative) {
        const result = await this.math(key, "-", value, goToNegative);
        return result;
    }

    /**
     * Array'a veri ekler.
     * @param {string} key Key
     * @param {any} value Value
     * @returns {Promise<any>}
     * @example db.push("test","succes");
     */
    async push(key, value) {
        const data = await this.get(key);
        if (!data) throw new Error(`DataBase'de ${key} ID'li veri yok`);
        if (!Array.isArray(data)) {
            return await this.set(key, [data]);
        } else {
            data.push(value);
            return await this.set(key, data);
        }
    }

    /**
     * Array'da value varmı yokmu kontrol eder.
     * @param {string} key Key
     * @param {any | any[]} value Value
     * @return {Promise<any>}
     * @example db.arrayHasValue("test",["succes","hello"]).then((result) => console.log(result));
     */
    async arrayHasValue(key, value) {
        const data = await this.get(key);
        if (!data) throw new Error(`DataBase'de ${key} ID'li veri yok`);
        if (!Array.isArray(data)) throw new Error(`DataBase'deki ${key} isimli veri array tipinde değil`);
        if (Array.isArray(value)) {
            const obj = {};
            value.forEach((item) => {
                const check = data.some((i) => i === item);
                if (check) obj[item] = true;
                else obj[item] = false;
            });
            return obj;
        }
        return data.some((item) => item === value);
    }

    /**
     * DataBase'de ID'lerin içinde belirtilen veri varsa o verileri getirir.
     * @param {string} key Key
     * @returns {Promise<Object>}
     * @example db.includes("te").then((obj) => console.log(obj));
     */
    async includes(key) {
        const obj = {};
        let keyArray = await this.keyArray();
        keyArray = keyArray.filter((item) => item.includes(key));
        if (keyArray.length < 1) return {};
        const json = await this.toJSON();
        for (const key of keyArray) {
            obj[key] = json[key];
        }
        return obj;
    }

    /**
     * DataBase'de ID'leri belirtilen veri ile başliyan verileri getir.
     * @param {string} key Key
     * @returns {Promise<Object>}
     * @example db.startsWith("te").then((obj) => console.log(obj));
     */
    async startsWith(key) {
        const obj = {};
        let keyArray = await this.keyArray();
        keyArray = keyArray.filter((item) => item.startsWith(key));
        if (keyArray.length < 1) return {};
        const json = await this.toJSON();
        for (const key of keyArray) {
            obj[key] = json[key];
        }
        return obj;
    }

    /**
     * DataBase'deki veriler üzerinde filtereleme yaparsınız.
     * @param {(value) => boolean} func Function
     * @returns {Promise<Collection>}
     * @exampke db.valueFilter((value) => value.includes("test")).then((items) => console.log(items));
     */
    async valueFilter(func) {
        if (!this.util.isFunction(func)) throw new Error(`Parametre fonksiyon olmak zorundadır.`);
        const allData = await this.toJSON();
        const result = new Collection();
        for (const item in allData) {
            if (func(allData[item])) result.set(item, allData[item]);
        }
        return result;
    }

    /**
     * DataBase'deki veriler üzerinde fonksiyona göre ilk çıkan veriyi getirir.
     * @param {(value) => boolean} func Function
     * @returns {Promise<any>} 
     * @exampke db.findValue((value) => value.includes("test")).then((item) => console.log(item));
     */
    async findValue(func) {
        return (await this.valueFilter(func)).first();
    }

    /**
     * DataBase'deki ID'ler üzerinde filtreleme yapar.
     * @param {(key) => boolean} func Function
     * @returns {Promise<Collection>}
     * @exampke db.keyFilter((keys) => keys.includes("test")).then((items) => console.log(items));
     */
    async keyFilter(func) {
        if (!this.util.isFunction(func)) throw new Error(`Parametre fonksiyon olmak zorundadır.`);
        const keys = await this.keyArray();
        const allData = await this.toJSON();
        const result = new Collection();
        if (keys.length < 1) return result;
        for (const key of keys) {
            if (func(key)) result.set(key, allData[key]);
        }
        return result;
    }

    
    /**
     * DataBase'deki ID'ler üzerinde fonksiyona göre ilk çıkan veriyi getirir.
     * @param {(value) => boolean} func Function
     * @returns {Promise<any>} 
     * @exampke db.findkey((keys) => keys.includes("test")).then((item) => console.log(item));
     */
    async findKey(func) {
        return (await this.keyFilter(func)).first();
    }


    /**
     * DataBase'deki ilk veriyi getirir.
     * @returns {Promise<any>}
     * @example db.firstValue().then((value) => console.log(value));
     */
    async firstValue() {
        return (await this.valueFilter(() => true)).first();
    }

    /**
     * DataBase'deki ilk ID'yi getirir.
     * @returns {Promise<any>}
     * @example db.firstKey().then((key) => console.log(key));
     */
    async firstKey() {
        return (await this.keyFilter(() => true)).first();
    }

    /**
     * Verileri Array olarak döner.
     * @param {number} [limit] Limit
     * @returns {Promise<any[]>}
     * @example db.array().then((items) => console.log(items));
     */
    async array(limit) {
        return await this.all(limit);
    }

    /**
     * Random veri döndürür.
     * @param {number} [amount] Amount
     * @returns {Promise<any>}
     * @example db.randomValue(3).then((randomValues) => console.log(randomValues));
     */
    async randomValue(amount = 0) {
        const valueArray = await this.valueArray();
        if (!amount) return Math.floor(Math.random() * valueArray.length);
        if (valueArray.length === 0 || !amount) return [];
        const result = Array.from({ length: amount }, () => {
            return valueArray.splice(Math.floor(Math.random() * valueArray.length), 1)[0];
        });
        return result.filter((item) => item !== undefined);
    }

    /**
     * Random ID döndürür.
     * @param {number} [amount] Amount
     * @returns {Promise<any>}
     * @example db.randomKey(3).then((randomKeys) => console.log(randomKeys));
     */
    async randomKey(amount = 0) {
        const keyArray = await this.keyArray();
        if (!amount) return Math.floor(Math.random() * keyArray.length);
        if (keyArray.length === 0 || !amount) return [];
        const result = Array.from({ length: amount }, () => {
            return keyArray.splice(Math.floor(Math.random() * keyArray.length), 1)[0];
        });
        return result.filter((item) => item !== undefined);
    }


    /**
     * Array içindeki verileri değiştirir.
     * @param {string} key Key
     * @param {(values) => boolean} func Function
     * @param {any} newValue
     * @returns {Promise<any>}
     * @example db.arrayChangeValue("test",(values => values > 10), 100);
     */
    async arrayChangeValue(key, func, newValue) {
        if (!this.util.isFunction(func)) throw new Error(`Parametre fonksiyon olmak zorundadır.`);
        const data = await this.get(key);
        if (!Array.isArray(data)) throw new Error(`${key} ID'li veri array tipinde değil.`);
        if (data.length < 1) return undefined;
        for (const item in data) {
            if (func(data[item])) data[item] = newValue;
        }
        return await this.set(key, data);
    }

    /**
     * @param {string} modelName
     * @returns {Promise<void>}
     * @example db.deleteModel("testModel");
     */
    async deleteModel(modelName) {
        if (modelName === "" || typeof modelName !== "string") {
            throw new Error("Model ismi string tipte olmak zorundadır.");
        }
        modelName.endsWith("s") ? void 0 : modelName = `${modelName}s`;
        await this.connection.dropCollection(modelName)
            .catch((e) => this.emit("error", e));
        return;
    }

    /**
     * @param {string} [modelName = "backup"]
     * @returns {Promise<this>}
     * @example db.backup("backupName");
     */
    async backup(modelName = "backup") {
        const allData = await this.fetchAll();
        const newDatabase = this.createModel(modelName);
        for await (const item of allData) {
            await newDatabase.set(item.ID, item.data);
        }
        return newDatabase;
    }

    /**
     * @param {string} [jsonFileName = "database.json"]
     * @returns {Promise<void>}
     * @example db.backupToJsonFile("database.json");
     */
    async backupToJsonFile(jsonFileName = "./database.json") {
        if (!jsonFileName.endsWith(".json")) throw new Error("JSON uzantılı bir dosya ismi girin.")
        const toJson = JSON.stringify((await this.toJSON()), null, 2);
        fs.writeFileSync(jsonFileName, toJson);
        return;
    }
}


module.exports = Database;



/**
 * @typedef {Database} database
 */



 /**
  * @typedef {Object} latencyType
  * @param {number} read
  * @param {number} write
  */
