const { Schema, Model } = require("mongoose");







class SchemaManager {
    /**
     * @param {DataBase} database
     */
    constructor(database) { 
        /**@type {DataBase} @private */
        this.database = database;
        /**@type {Schema} @private */
        this.schema = new Schema({
            ID: {
                // Tipi string.
                type: Schema.Types.String,
                // Doldurulması gerekli.
                required: true,
                // Benzeri olmiyan
                unique: true
            },
            data: {
                // Tipi any
                type: Schema.Types.Mixed,
                required: true
            }
        });
    }


    /**
     * @returns {Model}
     */
    createSchema() {
        return this.database
            .connection.model(this.database.modelName, this.schema);
    }

    /**
     * @param {string} key
     * @returns {Promise<boolean>}
     */
    async checkModel(key) {
        const data = await this.database.schema.findOne({ ID: key });
        return data ? true : false;
    }

    /**
     * @param {string} key
     * @returns {Promise<any>}
     */
    async getData(key) {
        const data = await this.database.schema.findOne({ ID: key });
        return data;
    }

    /**
     * @param {string} key Key
     * @returns {Promise<void>}
     */
    async findAndDelete(key) {
        const parsed = this.database.util.parseKey(key);
        this.database.schema.findOneAndDelete({ ID: parsed.key })
            .catch((e) => this.database.emit("error", e));
        return;
    }

    /**
     * @returns {Promise<void>}
     */
    async deleteAll() {
        await this.database.schema.deleteMany().catch((e) => this.database.emit("error", e));
        return;
    }
}



module.exports = SchemaManager;



/**
 * @typedef {import("./DataBase").database} DataBase
 */