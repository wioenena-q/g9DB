import { latencyType } from "../src/DataBase";
import { Collection } from "g9-collection";









declare module "g9db" {
    import { Collection } from "g9-collection";
    import { EventEmitter } from "events";
    import type {
      Connection,
      ConnectOptions,
      Model,
      Document,
      Schema
    } from "mongoose";
    

    // Class
    export class Base<V> extends EventEmitter {
      public constructor(mongodbURL: string,connectionOptions?: ConnectOptions);
      public util: Util;
      readonly mongodbURL:string;
      public options:ConnectOptions;
      public connection:Connection;
      public readyAt:Date;
      private _createConnection(url?:string):Connection;
      private _destroyDatabase():Promise<void>;
      private get url():string;
      private get state():string;
    }

    export class Database<V> extends Base<V> {
      public constructor(mongodbURL:string,name?:string,connectionOptions?:ConnectOptions);
      readonly modelName: string;
      readonly schemaManager: SchemaManager<V>;
      readonly schema: ModelType;
      private checkConnnection(): void;
      public disconnect(): Promise<void>;
      public set(key: string, value: V): Promise<V>;
      public get(key: string): Promise<V>;
      public fetch(key: string): Promise<V>;
      public exists(key: string): Promise<boolean>;
      public has(key: string): Promise<boolean>;
      public all(limit?: number): Promise<V[]>;
      public fetchAll(limit?: number): Promise<V[]>;
      public toJSON(limit?: number): Promise<Object>;
      public delete(key: string): Promise<void>;
      public deleteAll(): Promise<void>;
      public get uptime(): number;
      public connect(url: string): Promise<void>;
      public get name(): string;
      private readTimestamp(): Promise<number>;
      private writeTimestamp(): Promise<number>;
      public fetchLatency(): Promise<LatenctType>;
      public ping(): Promise<latencyType>;
      public type(key: string): Promise<GetType>;
      public pull(key: string, value: V | V[], multiple?: boolean): Promise<V>;
      public toString(): string;
      public createModel(name: string): this;
      public valueArray(): Promise<V>;
      public keyArray(): Promise<string[]>;
      public math(key: string, operator: operators, value: number, goToNegative?: boolean): Promise<V>;
      public add(key: string, value: number): Promise<V>;
      public substr(key: string, value: number, goToNegative?: boolean): Promise<V>;
      public push(key: string, value: V): Promise<V>;
      public arrayHasValue(key: string, value: any): Promise<boolean | Object>;
      public includes(key: string): Promise<Object>;
      public startsWith(key: string): Promise<Object>;
      public valueFilter(func: (value: V) => boolean): Promise<Collection<string, V>>;
      public findValue(func: (value: V) => boolean): Promise<V>;
      public keyFilter(func: (key: string) => boolean): Promise<Collection<string, V>>;
      public findKey(func: (key: string) => boolean): V;
      public firstValue(): Promise<V>;
      public firstKey(): Promise<string>;
      public array(): Promise<Object>;
      public randomValue(amount?: number): Promise<V | V[]>;
      public randomKey(amount?: number): Promise<string | string[]>;
      public arrayChangeValue(key: string, func: (values: any) => boolean, newValue: V): Promise<V>;
      public deleteModel(modelName: string): Promise<void>;
      public on<K extends keyof g9dbEvents>(event: K, listener: (...args: g9dbEvents[K]) => void): this;
        public on<S extends string | symbol>(event: Exclude<S, g9dbEvents>,listener: (...args: any[]) => void): this;
        
        
        public emit<K extends keyof g9dbEvents>(event: K, ...args: g9dbEvents[K]): boolean;
        public emit<S extends string | symbol>(
            event: Exclude<S, keyof g9dbEvents>,
            ...args: any[]
        ): boolean;


        public once<K extends keyof g9dbEvents>(event: K, listener: (...args: g9dbEvents[K]) => void): this;
        public once<S extends string | symbol>(
            event: Exclude<S, keyof g9dbEvents>,
            listener: (...args: any[]) => void
        ): this;


        public off<K extends keyof g9dbEvents>(event: K, listener: (...args: g9dbEvents[K]) => void): this;
        public off<S extends string | symbol>(
            event: Exclude<S, keyof g9dbEvents>,
            listener: (...args: any[]) => void
        ): this;


        public removeAllListeners<K extends keyof g9dbEvents>(event?: K): this;
        public removeAllListeners<S extends string | symbol>(event?: Exclude<S, keyof g9dbEvents>): this;
    }

    export class Util {
        public constructor();
        public isObject(item: any): boolean;
        public isString(item: any): boolean;
      public parseKey(key: string): { key: string, target?: string };
        public isNumber(item: any): boolean;
        public isFunction(item: any): boolean;
    }
  
    export class SchemaManager<V> {
      public constructor(database: DataBase<V>);
      private database: DataBase<V>;
      private schema: Schema;
      public createSchema(): Model;
      public checkModel(key: string): Promise<boolean>;
      public getData(key: string): Promise<any>;
      public findAndDelete(key: string): Promise<void>;
      public deleteAll():Promise<void>
    }
  
    export class g9dbError extends Error {
      public constructor(message: string, content?: string);
      public code: string | "TypeError";
      public get name(): string | "TypeError";
    }
  
    // Types
    export interface g9dbEvents {
      ready: [];
      debug: [any];
      error: [Error];
    }
    export type ModelType = Model<Document,{}>;
    export type LatenctType = {read:number,write:number};
    export type GetType = "string" | "number" | "bigint" | "boolean" | "symbol" | "array" | "undefined" | "object" | "function";
    export type operators = "+" | "-" | "*" | "/" | "%";
}