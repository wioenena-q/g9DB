const { Database } = require("../index");
const { "Yield-ModMongoDBURL":url } = require("../../global.json");
const db = new Database(url);


db.on("ready", async () => {
    await db.deleteModel("jsons");
});
