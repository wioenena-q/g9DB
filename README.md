![Image](https://nodei.co/npm/g9db.png?downloads=true&downloadRank=true&stars=true)

<br>

# Toplam indirme

![npm](https://img.shields.io/npm/dt/g9db?label=Total&style=for-the-badge)

# Yıllık indirme

![npm](https://img.shields.io/npm/dy/g9db?label=Total&style=for-the-badge)

# Aylık indirme

![npm](https://img.shields.io/npm/dm/g9db?label=Total&style=for-the-badge)

# Haftalık indirme

![npm](https://img.shields.io/npm/dw/g9db?label=Total&style=for-the-badge)

# Info

![NPM](https://img.shields.io/npm/l/g9db?color=cyan&label=Licance&style=for-the-badge)
![npm bundle size](https://img.shields.io/bundlephobia/min/g9db?color=cyan&label=size&style=for-the-badge)
![npm](https://img.shields.io/npm/v/g9db?color=cyan&label=version&style=for-the-badge)
![Image](https://img.shields.io/badge/Yap%C4%B1mc%C4%B1-wioenena.q-cyan?style=for-the-badge)

# g9db

## İndirmek için || Download

```bash
npm i g9db
```

# Support

- TypeScript
- Mobile ide
- Glitch
- Windows
- Linux
- Heroku

# NEWS 👑

- Artık tamamen mongodb tabanlı çalışıyor.

# Kullanım

```javascript
const { Database } = require("g9db");
const db = new Database("mongodbURL");
(async () => {
const set = await db.set("test","succes");

const get = await db.get("test");

const fetch = await db.fetch("test");

const has = await db.exists("test");

const has = await db.has("test");

const all = await db.all(5);

const allData = await db.fetchAll();

const toJson = await db.toJSON();

await db.delete("test")

await db.deleteAll();

await db.connect("mongodbURL");

const type = await db.type("test");

await db.pull("test","hello");

const vallArr = await db.valueArray();

const keyArr = await db.keyArray();

await db.math("test","/",5);

await db.add("test",5);

await db.substr("test",2);

await db.push("test","succes");

const arrayHas = await db.arrayHasValue("test",["succes","hello"]);

const includes = await db.includes("te");

const startsWith = await db.startsWith("te");

const valueFilter = await db.valueFilter((value) => value.includes("test"));

const values = await db.findValue((value) => value.includes("test"));

const keyFilter = await db.keyFilter((keys) => keys.includes("test"));

const item = await db.findkey((keys) => keys.includes("test"));

const value = await db.firstValue();

const key = await db.firstKey();

const items = await db.array();

const randomValues = await db.randomValue(3);

const randomKeys = await db.randomKey(3);

await db.arrayChangeValue("test",(values => values > 10), 100);

await db.deleteModel("testModel");

await db.backup("backupName");

await db.backupToJsonFile("database.json")

}();
```

# Contact

[İnstagram](https://www.instagram.com/wioenena.q/)

- Discord: g9#0001
